## Instructions
1. Remove  <engine name="windows-sdk" version=">=10.0.14393.0" /> inside plugin cordova-plugin-background-mode from plugin.xml to solve plugin installation issue

**Build issue. ERR FILE NOT FOUND error**
    Solution: Run following commands
    1. ionic cordova platform remove android
    2. ionic platform add android@latest
    3. ionic cordova resource
    4. ionic cordova run android

**For geo location, please refer these links**
    https://www.npmjs.com/package/cordova-background-geolocation-lt
    https://github.com/transistorsoft/cordova-background-geolocation-lt/tree/master/help

**Database Location**
    https://console.firebase.google.com/project/track360-b6281/database/firestore/data~2Ftrack_numbers~2F1rVo3HCSbzAzYExUoIaP

