// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBHbOVbKCmu8Bp_YmiEEsBEd-6adeqtXrc',
    authDomain: 'track360-b6281.firebaseio.com',
    databaseURL: 'https://track360-b6281.firebaseio.com',
    projectId: 'track360-b6281',
    storageBucket: 'track360-b6281.appspot.com',
    messagingSenderId: '125970832881'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
