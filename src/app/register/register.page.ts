import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

import { Constants } from '../utils/constants';
import { MustMatch } from '../helpers/must-match.validator';
import { AppService } from '../service/app.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder, 
    private db: AngularFirestore, 
    private afAuth: AngularFireAuth,
    private appService: AppService
    ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      mobile: ['', [Validators.required]],
      name: ['', [Validators.required]],
      email: ['', []],
      password: ['', Validators.required],
      confirmPassword: ['', [Validators.required]]
    }, {
        validator: MustMatch('password', 'confirmPassword')
      });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      console.log('Form is invalid');
    } else {
      this.appService.showLoading(true);
      console.log('Form is valid');
      var obj = this.registerForm.value;
      this.afAuth.auth.createUserWithEmailAndPassword(obj.email, obj.password).then(res => {
        console.log("Create user res = ",res);
        const uId = res.user.uid;
        res.user.updateProfile({displayName: obj.name}).then(res => {
          /**
           * Deleting password and confirmPassword keys from json object as we don't want 
           * to save password in the `users` collections
           **/
          delete obj['confirmPassword'];
          delete obj['password'];
          const currDateTime = Constants.getCurrDate();
          obj.createdDate = currDateTime;
          obj.changedDate = currDateTime;
          this.db.collection('users').doc(uId).set(obj);
          this.registerForm.reset();
          this.appService.showLoading(false);
          this.appService.showAlert(true, 'Succeed', 'You have successfully registered with '+Constants.APP_NAME+'. Please login and enjoy!!');
        }, err => {
          this.appService.showLoading(false);
          console.log('Failed to update user displayName');
        });
      }, err => {
        this.appService.showLoading(false);
        console.log('Authentication Error = ', err);
      });
    }
  }

}
