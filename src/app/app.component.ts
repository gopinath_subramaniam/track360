import { Component } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppService } from './service/app.service';
import { MessageService } from './service/message.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  loggedInUsername: string = 'Anonymous';

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navCtrl: NavController,
    private appService: AppService,
    private messageService: MessageService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.appService.enableSideMenu(false);
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    
    this.platform.resume.subscribe(() => {
      console.log('Resumed');
    });

    this.platform.pause.subscribe(() => {
      console.log('paused');
    });
    
    this.messageService.listen().subscribe(displayName => {
      this.loggedInUsername = displayName;
    });
  }

  logout() {
    this.appService.enableSideMenu(false);
    localStorage.clear();
    this.navCtrl.navigateRoot('/login');
  }


  static displayUsername() {
    console.log('Display username called');

  }
}
