export class TrackNumber{
    id: string;
    trackerId: string;
    userId: string;
    name: string;
    mobile: string;

    constructor(init?: Partial<TrackNumber>) {
        Object.assign(this, init);
    }
}