export class LocationHistory {
    userId: string;
    createdDate: string;
    latitude: number;
    longitude: number;
}