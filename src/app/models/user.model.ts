import { Timestamp } from 'rxjs';

export class UserModel {
    uId: string;
    name: string;
    userName: string;
    mobile: string;
    email: string;
    latitude: string;
    longitude: string;
    createdDate: string;
    changedDate: string;
    
    constructor(init?: Partial<UserModel>) {
        Object.assign(this, init);
    }
}