import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TrackNumber } from '../models/track.number.model';

import { AppService } from '../service/app.service';
import { PersonListService } from './person-list.service';
import { Constants } from '../utils/constants';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.page.html',
  styleUrls: ['./person-list.page.scss'],
})

export class PersonListPage implements OnInit {
  trackNums: Observable<TrackNumber[]>;

  constructor(
    private appService: AppService,
    private personListService: PersonListService
    ) { }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.appService.showLoading(true);
    this.trackNums = this.personListService.getAllPersons();
    const trackNumSub = this.trackNums.subscribe(res => {
      trackNumSub.unsubscribe();
      this.appService.showLoading(false);
    }, err => {
      this.appService.showLoading(false);
    });
  }

  removePerson(id:string){
    const that = this;
    this.appService.showConfirm(Constants.MSG_TYPE.confirmation, Constants.MSG_ERROR.sureWantToDelete).then(canDelete => {
      if(canDelete && canDelete === true){
        that.appService.showLoading(true);
        that.personListService.removePerson(id).then(res => {
          that.appService.showAlert(true, 'Success', 'Deleted successfully');
          that.appService.showLoading(false);
        }).catch(err => {
          that.appService.showAlert(true, 'Error', 'Error while deleting. Please try again');
          that.appService.showLoading(false);
        });
      }
    });
  }
  
}
