import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

import { TrackNumber } from '../models/track.number.model';
import { Constants } from '../utils/constants';


@Injectable({
  providedIn: 'root'
})
export class PersonListService {
  
  constructor(private db: AngularFirestore) { }

  getAllPersons() {
    const uId = Constants.getUID();
    return this.db.collection<TrackNumber>(Constants.COLLECTION.track_numbers, ref => ref.where('trackerId', '==', uId)).snapshotChanges()
    .pipe(map( actions => actions.map(a => {
      const data = a.payload.doc.data();
      data.id = a.payload.doc.id;
      return data;
    })));
  }

  removePerson(id:string){
    return this.db.collection<TrackNumber>(Constants.COLLECTION.track_numbers).doc(id).delete();
  }
}
