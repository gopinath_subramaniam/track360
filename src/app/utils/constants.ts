export class Constants {
    static APP_NAME: string = 'Track360';
    static COLLECTION = {users: 'users', track_numbers: 'track_numbers', location_history: 'location_history'};
    static LOC_CHANGE_COUNT:number = 5;

    static KEYS_STORAGE = {DISPLAY_NAME: 'displayName'};

    static uid: string = 'uid';
    static displayName: string = 'displayName';
    static email: string = 'email';

    static MAIN_MENU_NAME: string = 'mainMenu';

    static STATUS = {fail: 'FAIL', ok:'OK'};
    
    static MSG_TYPE = {confirmation: 'Confirmation'};
    static MSG_ERROR = {sureWantToDelete: 'Are you sure that you want to delete this person' };

    public static toDateString(date: Date): string {
        return (date.getFullYear().toString() + '-' 
           + ("0" + (date.getMonth() + 1)).slice(-2) + '-' 
           + ("0" + (date.getDate())).slice(-2))
           + ' ' + date.toTimeString().slice(0,5);
    }

    public static getCurrDate(): string {
        const date = new Date();
        return (date.getFullYear().toString() + '-' 
           + ("0" + (date.getMonth() + 1)).slice(-2) + '-' 
           + ("0" + (date.getDate())).slice(-2))
           + ' ' + date.toTimeString().slice(0,5);
    }

    public static getErrorJsonObj(msg:string){
        return {status: this.STATUS.fail, msg: msg};
    }
    

    public static getUID(){
        return localStorage.getItem(Constants.uid);
    }


}