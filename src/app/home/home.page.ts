import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { AppService } from '../service/app.service';
import { LocationService } from '../service/location.service';
import { MessageService } from '../service/message.service';
import { Constants } from '../utils/constants';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    private appService: AppService,
    private locService: LocationService,
    private messageService: MessageService
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    console.log('Home Page called');
    this.appService.enableSideMenu(true);
    this.locService.checkGPSPermission();
    this.locService.startBackgroundLocationService();
    const displayName = localStorage.getItem(Constants.KEYS_STORAGE.DISPLAY_NAME);
    this.messageService.filter(displayName);
  }

  showPopover(ev) {
    this.appService.showPopover(ev);
  }

}
