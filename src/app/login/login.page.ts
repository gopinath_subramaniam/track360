import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';

import {Constants} from '../utils/constants';
import { AppService } from '../service/app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  notLoggedIn: boolean = false;
  loginForm:FormGroup;

  constructor(private formBuilder: FormBuilder, 
              private afAuth: AngularFireAuth,
              private navCtrl: NavController,
              private appService: AppService
              ) { }

  ngOnInit() {
    debugger;
    const uId = Constants.getUID();
    if(uId){
      this.navCtrl.navigateRoot('/home');
    }else{
      this.notLoggedIn = true;
      this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required]],
        password: ['', Validators.required]
      });
    }
  }

  loginSubmit(){  
    console.log('login submit called');
    if(this.loginForm.invalid){

    }else{
      this.appService.showLoading(true);
      var obj = this.loginForm.value;
      debugger;
      this.afAuth.auth.signInWithEmailAndPassword(obj.email, obj.password).then(res => {
        console.log('signInWithEmailAndPassword = ', res);
        localStorage.setItem(Constants.uid, res.user.uid);
        localStorage.setItem(Constants.displayName, res.user.displayName);
        localStorage.setItem(Constants.email, res.user.email);
        this.loginForm.reset();
        this.appService.showLoading(false);
        //this.appService.showAlert(true, 'Succeed', 'You have successfully registered with '+Constants.APP_NAME+'. Please login and enjoy!!');
        this.navCtrl.navigateRoot('/home');
      }, err => {
        this.appService.showLoading(false);
        console.log('Login Error Occurred = ', err);
        let errMsg = 'Invalid Username or Password';
        if(err.code.indexOf('user-not-found') > -1){
          errMsg = 'User Not Found';
        }else if(err.code.indexOf('wrong-password') > -1){
          errMsg = 'Wrong Password';
        }else if(err.code.indexOf('invalid-email') > -1){
          errMsg = 'Invalid Email';
        }
        this.appService.showAlert(true, 'Login Error', errMsg);
      })
    }
    
  }
}
