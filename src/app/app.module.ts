import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import BackgroundFetch from 'cordova-plugin-background-fetch';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation/ngx';

import { AppService } from './service/app.service';
import { LocationService } from './service/location.service';
import { MessageService } from './service/message.service';

import { PopoverPage } from './directives/popover/popover.page';

@NgModule({
  declarations: [AppComponent, PopoverPage],
  entryComponents: [PopoverPage],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    BackgroundMode,
    AndroidPermissions,
    Geolocation,
    BackgroundGeolocation,
    LocationAccuracy,
    BackgroundFetch,
    AppService,
    LocationService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
