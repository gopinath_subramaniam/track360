import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { AppService } from '../service/app.service';
import { PersonAddService } from './person-add.service';

import { Constants } from '../utils/constants';

@Component({
  selector: 'app-person-add',
  templateUrl: './person-add.page.html',
  styleUrls: ['./person-add.page.scss'],
})
export class PersonAddPage implements OnInit {

  personAddForm: FormGroup;
  submitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private appService: AppService,
    private personAddService: PersonAddService
  ) {
  }

  ngOnInit() {
    this.personAddForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      mobile: ['', [Validators.required]]
    });
  }

  get f() { return this.personAddForm.controls; }
  
  onSubmit() {
    console.log('person add submit called');
    if (this.personAddForm.invalid) {
      console.log('Invalid person add form');
    } else {
      this.appService.showLoading(true);
      var that = this;
      this.personAddService.createPersonToTrack(that.personAddForm.value).then(res => {
        console.log('Res = ', res);
        this.personAddForm.reset();
        that.appService.showAlert(true, 'Succeed', 'Person added successfully');
        that.appService.showLoading(false);
      }, err => {
        console.log('Error Occurred = ', err);
        let errMsg = 'Failed to add person';
        if (err.msg) errMsg = err.msg;
        that.appService.showAlert(true, 'Failed', errMsg);
        that.appService.showLoading(false);
      });
    }
  }

}
