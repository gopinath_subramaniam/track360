import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonAddPage } from './person-add.page';

describe('PersonAddPage', () => {
  let component: PersonAddPage;
  let fixture: ComponentFixture<PersonAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonAddPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
