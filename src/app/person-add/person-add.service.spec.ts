import { TestBed } from '@angular/core/testing';

import { PersonAddService } from './person-add.service';

describe('PersonAddService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PersonAddService = TestBed.get(PersonAddService);
    expect(service).toBeTruthy();
  });
});
