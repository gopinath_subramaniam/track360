import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { Constants } from '../utils/constants';

@Injectable({
  providedIn: 'root'
})
export class PersonAddService {

  constructor(
    private db: AngularFirestore
  ) { }


  checkPersonExistsOrNot(mobile: string) {
    const trakcNumDoc = this.db.collection(Constants.COLLECTION.users).ref.where('mobile', '==', mobile);
    return trakcNumDoc;
  }

  createPersonToTrack(obj: any) {
    const promise = new Promise((resolve, reject) => {
      const uId = Constants.getUID();
      obj.trackerId = uId;
      this.checkPersonExistsOrNot(obj.mobile).get().then(docs => {
        console.log('checkPersonExistsOrNot = ', docs.docs[0]);
        const userDoc = docs.docs[0];
        if (userDoc && userDoc.exists) {
          const userId = userDoc.id;
          if(userId != uId){
            obj.userId = userId;
            const trackNumsColl = this.db.collection(Constants.COLLECTION.track_numbers);
            trackNumsColl.ref.where('mobile', '==', obj.mobile).where('trackerId', '==', uId).get().then(trackNums => {
              if(trackNums.docs.length > 0){
                reject(Constants.getErrorJsonObj('Number already exists'));
              }else{
                this.db.collection(Constants.COLLECTION.track_numbers).add(obj).then(res => {
                  resolve(res);
                }).catch(err => {
                  reject(err);
                });
              }
            }).catch(err =>{
              reject(err);
            })
          }else{
            reject(Constants.getErrorJsonObj('Cannot add the same number you are using now'));
          }
        } else {
          reject(Constants.getErrorJsonObj('Mobile number is not registered'));
        }
      });
    });
    return promise;
  }

  addPersonToColl(){

  }

}
