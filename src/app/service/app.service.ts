import { Injectable } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

import { PopoverController } from '@ionic/angular';
import { PopoverPage } from '../directives/popover/popover.page';
import { AlertOptions } from '@ionic/core';

@Injectable()
export class AppService {
  
  isLoading = false;
  popoverMenu:any;
  loadingModal: any;
  alertModal: any;

  constructor(private menuCtrl: MenuController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public popoverCtrl: PopoverController
  ) { }

  enableSideMenu(val: boolean) {
    this.menuCtrl.enable(val);
  }

  async showLoading(val: boolean) {
    if (val == true) {
      this.isLoading = true;
      this.loadingModal = await this.loadingCtrl.create({
        message: 'Please Wait',
      }).then(a => {
        a.present().then(() => {
          if (!this.isLoading) {
            a.dismiss();
          }
        });
      });
      //await this.loadingModal.present();
    } else {
      this.isLoading = false;
      if(this.loadingModal){
        await this.loadingModal.dismiss();
      }else{
        await this.loadingCtrl.dismiss();
      }
    }
  }

  async showAlert(val: boolean, title: string, msg: string) {
    if (val) {
      this.alertModal = await this.alertCtrl.create({
        // header: 'Confirm',
        subHeader: title,
        message: msg,
        buttons: ['OK']
      });
      await this.alertModal.present();
    } else {
      if(this.alertModal){
        await this.alertModal.dismiss();
      }else{
        await this.alertCtrl.dismiss();
      }
      
    }
  }

  showConfirm(title:string, msg: string){
    var promise = new Promise((resolve, reject) => {
      let alertOptionObj: AlertOptions = {
        subHeader: title,
        message: msg,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              resolve(false);
            }
          },
          {
            text: 'Ok',
            handler: () => {
              resolve(true);
            }
          }
        ]
      };
      const alert = this.alertCtrl.create(alertOptionObj);
      alert.then((alertObj) => {
        alertObj.present();
      });
    });
    
    return promise;
  }

  async showPopover(ev: any) {
    this.popoverMenu = await this.popoverCtrl.create({
      component: PopoverPage,
      event: ev,
      translucent: true
    });
    this.popoverMenu = await this.popoverMenu.present();
    return this.popoverMenu;
  }
  
  async hidePopover() {
    await this.popoverCtrl.dismiss();
  }

}
