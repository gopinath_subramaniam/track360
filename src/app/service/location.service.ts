import { Injectable, NgZone } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Subscription } from 'rxjs';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation, GeolocationOptions } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import BackgroundFetch, { BackgroundFetchConfig } from "cordova-plugin-background-fetch";
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse, BackgroundGeolocationEvents, BackgroundGeolocationAccuracy } from '@ionic-native/background-geolocation/ngx';

import { LocationHistory } from '../models/location.history';
import { UserModel } from '../models/user.model';
import { Constants } from '../utils/constants';

@Injectable()
export class LocationService {

  static foreGroundSubscription: Subscription;
  static currentPos: any;

  constructor(
    private zone: NgZone,
    private afAuth: AngularFirestore,
    private backgroundMode: BackgroundMode,
    private geolocation: Geolocation,
    private androidPermissions: AndroidPermissions,
    private locationAccuracy: LocationAccuracy,
    private backgroundGeolocation: BackgroundGeolocation
  ) { }

  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {
          console.log('GPS Permission');
          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {

          //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        console.log('GPSLocation accuracy');
        // When GPS Turned ON call method to get Accurate location coordinates
        this.startForegroundLocationService();
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  startForegroundLocationService(){
    const config: GeolocationOptions = {enableHighAccuracy: true};
    LocationService.foreGroundSubscription = this.geolocation.watchPosition(config).subscribe(loc => {
      this.updateLocationInDatabase(loc.coords);
    });
  }

  stopForegroundLocationService(){
    if(LocationService.foreGroundSubscription){
      LocationService.foreGroundSubscription.unsubscribe();
    }
  }

  startBackgroundLocationService() {
    this.backgroundGeolocation.configure({
      locationProvider: 0,
      desiredAccuracy: 0,
      stationaryRadius: 50,
      distanceFilter: 50,
      notificationTitle: 'Background tracking',
      notificationText: 'enabled',
      debug: true,
      interval: 10000
    });
  
    this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe(loc => {
      // handle your locations here
      // to perform long running operation on iOS
      // you need to create background task
      console.log('Location Changed =', loc);
      this.updateLocationInDatabase(loc);
    });
  
    this.backgroundGeolocation.on(BackgroundGeolocationEvents.stationary).subscribe(stationaryLocation => {
      // handle stationary locations here
      console.log('Stationary Location =', stationaryLocation);
    });
  
    this.backgroundGeolocation.on(BackgroundGeolocationEvents.error).subscribe(err => {
      console.log('[ERROR] BackgroundGeolocation error:', err);
    });

    this.backgroundGeolocation.checkStatus().then(status => {
      console.log('[INFO] BackgroundGeolocation service is running', status.isRunning);
      console.log('[INFO] BackgroundGeolocation services enabled', status.locationServicesEnabled);
      console.log('[INFO] BackgroundGeolocation auth status: ' + status.authorization);
  
      // you don't need to check status before start (this is just the example)
      if (!status.isRunning) {
        this.backgroundGeolocation.start(); //triggers start on start event
      }
    });
    this.configureBackgroundLocation();
  }
  private configureBackgroundLocation() {
    this.backgroundMode.setDefaults({
      title: 'Track360',
      text: 'Tracking Location'
    });
    this.backgroundMode.enable();
    this.backgroundMode.on('activate').subscribe(() => {
      console.log('Background mode activated');
      this.backgroundMode.disableWebViewOptimizations();
      this.stopForegroundLocationService();
      this.startBackgroundLocationService();
    });
  }

  private updateLocationInDatabase(location: any) {
    const oldPos = LocationService.currentPos;
    const newPos = location;
    const currDate = Constants.getCurrDate();
    const valid = (oldPos && (oldPos.latitude !== newPos.latitude || oldPos.longitude !== newPos.longitude) && oldPos.changedDate !== currDate);
    console.info('Current Date = ', currDate);
    if ((!oldPos && newPos.latitude !== 0 && newPos.longitude !== 0) || valid) {
      const loc = { latitude: newPos.latitude, longitude: newPos.longitude, changedDate: currDate };
      this.createLocationHistory(loc);
      const uId = Constants.getUID();
      this.afAuth.collection<UserModel>(Constants.COLLECTION.users).doc(uId).set(loc, { merge: true }).then(res => {
        console.info('User location updated successfully');
        LocationService.currentPos = loc;
      });
    }
  }

  private createLocationHistory(location: any) {
    const uId = Constants.getUID();
    location.userId = uId;
    this.afAuth.collection<LocationHistory>(Constants.COLLECTION.location_history).add(location).then(res => {
      console.info('Location history inserted successfully');
    });
  }




}
