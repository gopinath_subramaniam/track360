import { NgModule } from '@angular/core';


import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  //{ path: 'person-add', loadChildren: './person-add/person-add.module#PersonAddPageModule' },
  //{ path: 'popover', loadChildren: './directives/popover/popover.module#PopoverPageModule' },
  //{ path: 'person-list', loadChildren: './person-list/person-list.module#PersonListPageModule' },
  // { path: 'map', loadChildren: './map/map.module#MapPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
