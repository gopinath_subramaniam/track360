import { Component, OnInit } from '@angular/core';
import { MapService } from './map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  
  constructor(private mapService: MapService) { }

  ionViewDidLoad() {
  }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.mapService.loadMapMarkers();
  }

}
