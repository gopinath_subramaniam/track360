import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment,
  GoogleMapsAnimation
} from '@ionic-native/google-maps';
import { Observable, Subscription } from 'rxjs';

import { AppService } from '../service/app.service';

import { TrackNumber } from '../models/track.number.model';
import { UserModel } from '../models/user.model';
import { Constants } from '../utils/constants';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  static subscribeLoc: Subscription;
  mapReady: Boolean = false;
  static map: GoogleMap;
  markers: any;// Array<GoogleMapOptions>;
  subscribeList: Array<any> = [];
  constructor(private db: AngularFirestore, private appService: AppService) { }

  loadMapMarkers() {
    if (!MapService.map) {
      this.createMap(20.5937, 78.9629);
    }
    this.deleteMarkers();
    this.getAllPersons();
  }

  getAllPersons() {
    this.appService.showLoading(true);
    this.unsubscribeAll();
    const uId = Constants.getUID();
    const trackSubscribe = this.db.collection<TrackNumber>(Constants.COLLECTION.track_numbers, ref => ref.where('trackerId', '==', uId)).get().subscribe(res => {
      trackSubscribe.unsubscribe();
      const that = this;
      let count = 0;
      let valid = false;
      res.docs.forEach(function (resObj: any, i) {
        const obj = resObj.data();
        MapService.subscribeLoc = that.db.collection<UserModel>(Constants.COLLECTION.users).doc(obj.userId).valueChanges().subscribe((userObj:any) => {
          if(!valid){
            count++;
          }
          if(!valid && res.docs.length == count){
            valid = true;
            MapService.map.setCameraTarget({lat: userObj.latitude, lng: userObj.longitude});
            MapService.map.animateCameraZoomIn();
          }
          userObj.name = obj.name;
          that.addLocationToMarkers(userObj);
          if(valid){
            that.showMarkersInMap();
          }
        }, err => {
          count++;
        });
        //that.subscribeList.push(userLocChangeSubscribe);
      });
      that.appService.showLoading(false);
    }, err => {
      console.log('Occurred Error = ', err);
      this.appService.showLoading(false);
    });
  }

  createMap(lat: number, lng: number) {
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: lat,
          lng: lng
        },
        zoom: 4,
        tilt: 30
      },
      center: { lat: lat, lng: lng }
    };
    MapService.map = GoogleMaps.create('map_canvas', mapOptions);
    MapService.map.setMyLocationEnabled(true);
  }

  addLocationToMarkers(obj: any) {
    var exists = false;
    var existingMarker = this.markers.forEach((item, i) => {
      if(item.mobile == obj.mobile){
        exists = true;
        let latLngObj = { lat: obj.latitude, lng: obj.longitude };
        this.markers[i].title = obj.name;
        this.markers[i].center = latLngObj;
        this.markers[i].position = latLngObj;
      }
    });
    if(!exists){
      let latLngObj = { lat: obj.latitude, lng: obj.longitude };
      let mapOptions: MarkerOptions = {
        mobile: obj.mobile,
        title: obj.name,
        camera: {
          zoom: 15,
          tilt: 30
        },
        center: latLngObj,
        position: latLngObj,
        animation: GoogleMapsAnimation.DROP
      };
      this.markers.push(mapOptions);
    }
  }

  showMarkersInMap() {
    if(MapService.map) MapService.map.clear();
    console.log('Show markers in map');
    const makersCluster = MapService.map.addMarkerClusterSync({
      markers: this.markers,
      icons: [],
      current: this.markers[0]
    });
    makersCluster.on(GoogleMapsEvent.MAP_READY).subscribe((params: any) => {
      let marker: Marker = <Marker>params[1];
      console.log('Marker = ', marker);
      marker.showInfoWindow();
    });
  }

  deleteMarkers(){
    if(MapService.map) MapService.map.clear();
    this.markers = [];
  }

  unsubscribeAll(){
    /* if(this.subscribeList){
      this.subscribeList.forEach(item => {
        if(item && item.unsubscribe && !item.closed){
          item.unsubscribe();
        }
      });
      this.subscribeList = [];
    } */
    if(MapService.subscribeLoc && !MapService.subscribeLoc.closed){
      MapService.subscribeLoc.unsubscribe();
    }

  }

}
